package model;

import java.io.Serializable;

public class TextItem extends Item implements Serializable {
    private String itemText;
    
    /**
     *
     */
    public TextItem() {
        super();
        this.itemText = null;
    }
    
    /**
     *
     * @param itemTitle
     * @param itemText
     */
    public TextItem(String itemTitle, String itemText) {
        super(itemTitle);
        this.itemText = itemText;
    }
    
    /**
     * @return the itemText
     */
    public String getItemText() {
        return itemText;
    }

    /**
     * @param itemText the itemText to set
     */
    public void setItemText(String itemText) {
        this.itemText = itemText;
    }
    
    @Override
    public String toString() {
        return super.toString() +
                " - Text: " + getItemText();
    }

    @Override
    String toString(char delimiter) {
        String output = QUOTE + super.getItemTitle() + QUOTE + delimiter +
                        QUOTE + getItemText() + EOLN;
        return output;
    }

    
}
