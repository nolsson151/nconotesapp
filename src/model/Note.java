package model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.function.Predicate;
import repositories.RepositoryObject;

public class Note extends RepositoryObject implements Comparable<Note>, Serializable {
    
    static final char EOLN='\n';       
    static final String QUOTE="\"";
    
    private int id;
    private String noteHeading;
    private String noteSubject;
    private Calendar dateCreated;
    private Calendar dateModified;
    private ArrayList<Item> noteItems;
    
    private static int lastNoteIdAllocated = 0;   
    
    /**
     *
     */
    public Note() {
        this.id = ++lastNoteIdAllocated;        
        this.noteItems = new ArrayList<>();
        this.noteHeading = "No Heading";
        this.noteSubject = "No Subject";        
        this.dateCreated = getNow();
        this.dateModified = getNow();
    }
    
    /**
     *
     * @param noteHeading
     * @param noteSubject
     */
    public Note(String noteHeading, String noteSubject) {
        this.id = ++lastNoteIdAllocated;           
        this.noteItems = new ArrayList<>();
        this.noteHeading = noteHeading;
        this.noteSubject = noteSubject;        
        this.dateCreated = getNow();
        this.dateModified = getNow();
    }    
    
    /**
     *
     * @param noteId
     * @param noteHeading
     * @param noteSubject
     * @param dateCreated
     * @param dateModified
     */
    public Note(int noteId, String noteHeading, String noteSubject, Calendar dateCreated, Calendar dateModified) {
        this.id = noteId;
        this.noteItems = new ArrayList<>();
        this.noteHeading = noteHeading;
        this.noteSubject = noteSubject;         
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
        if (noteId > Note.lastNoteIdAllocated)
            Note.lastNoteIdAllocated = noteId;
    }    
    
    /**
     *
     * @return
     */
    @Override
    public int getId() { 
        return this.id;
    }
    
    // Methods required: getters, setters, hashCode, equals, compareTo, comparator
    
    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the noteHeading
     */
    public String getNoteHeading() {
        return noteHeading;
    }

    /**
     * @param noteHeading the noteHeading to set
     */
    public void setNoteHeading(String noteHeading) {
        this.noteHeading = noteHeading;
    }

    /**
     * @return the noteSubject
     */
    public String getNoteSubject() {
        return noteSubject;
    }

    /**
     * @param noteSubject the noteSubject to set
     */
    public void setNoteSubject(String noteSubject) {
        this.noteSubject = noteSubject;
    }

    /**
     * @return the dateCreated
     */
    public Calendar getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Calendar dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the dateModified
     */
    public Calendar getDateModified() {
        return dateModified;
    }

    /**
     * @param dateModified the dateModified to set
     */
    public void setDateModified(Calendar dateModified) {
        this.dateModified = dateModified;
    }

    /**
     * @return the noteItems
     */
    public ArrayList<Item> getNoteItems() {
        return noteItems;
    }
    
    public int getNoItems(){
        return this.noteItems.size();
    }

    /**
     * @param noteItems the noteItems to set
     */
    public void setNoteItems(ArrayList<Item> noteItems) {
        this.noteItems = noteItems;
    }
    
    /**
     *
     * @param newItem
     */
    public void addItem(Item newItem) {
        this.getNoteItems().add(newItem);
        this.setDateModified(getNow());        
    }

    /**
     *
     * @param newItem
     * @param index
     */
    public void updateItem(Item newItem, int index) {
        this.getNoteItems().set(index, newItem);
        this.setDateModified(getNow());           
    }
    
    /**
     *
     * @param index
     */
    public void removeItem(int index) {
        this.getNoteItems().remove(index);
        this.setDateModified(getNow());           
    }     
    
    private Calendar getNow() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar now = Calendar.getInstance();  
        return now;
    }
    
    private String formatDate(Calendar calendar) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar now = Calendar.getInstance();  
        return dateFormat.format(calendar.getTime());
    }
    
    @Override
    public String toString() {
        return "\nNote Id: " + id + " - Note Heading: " + getNoteHeading() +
                " - Subject: " + getNoteSubject() +                
                " - Created: " + formatDate(getDateCreated()) +
                " - Modified: " + formatDate(getDateModified()) +
                "\nItems: " + getNoteItems().toString();
    }
    
    public String toString(char delimiter){
        String output = Integer.toString(id) + delimiter +
                        QUOTE + getNoteHeading() + QUOTE + delimiter +
                        QUOTE + getNoteSubject() + QUOTE + delimiter +
                        QUOTE + getDateCreated() + QUOTE + delimiter + 
                        QUOTE + getDateModified() + QUOTE + delimiter +
                        QUOTE + getNoteItems().toString() + EOLN;
                
        return output;
    }
    
    @Override
    public int hashCode(){
        return getId() * 31 + getNoteHeading().hashCode() * 31 + getNoteSubject().hashCode() * 31 + getDateCreated().hashCode() * 31 
                + getDateModified().hashCode() * 31 + getNoteItems().hashCode() * 31;
                
    }
    
    @Override
    public boolean equals(Object o){
        if(o instanceof Note){
            Note n = (Note)o;
            return getId() == getId() &&
                   getNoteHeading() == getNoteHeading() &&
                   getNoteSubject() == getNoteSubject() &&
                   getDateCreated() == getDateCreated() &&
                   getDateModified() == getDateModified() &&
                   getNoteItems() == getNoteItems();             
        } else{
            return false;
        }
    }

    @Override
    public int compareTo(Note o) {
        int noteId = ((Note) o).getId();
        
        return this.id - noteId;
    }

    public static Comparator<Note> DateModifiedComparator = new Comparator<Note>() {

        @Override
        public int compare(Note note1, Note note2) {
            Calendar dateModified1 = note1.getDateModified();
            Calendar dateModified2 = note2.getDateModified();
            
            return dateModified2.compareTo(dateModified1);
        }
    };
   
    
    public boolean match(Predicate<Note> tester){
        return tester.test(this);
    }
    
    
}
