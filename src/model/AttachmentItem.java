package model;

import java.io.Serializable;

public class AttachmentItem extends Item implements Serializable {
    private String itemFileName;
    private String itemFileType;
    
    /**
     *
     */
    public AttachmentItem() {
        super();
        this.itemFileName = null;
        this.itemFileType = null;        
    }
    
    /**
     *
     * @param itemTitle
     * @param itemFileName
     * @param itemFileType
     */
    public AttachmentItem(String itemTitle, String itemFileName, String itemFileType) {
        super(itemTitle);
        this.itemFileName = itemFileName;
        this.itemFileType = itemFileType;        
    }
    
    /**
     * @return the itemFileName
     */
    public String getItemFileName() {
        return itemFileName;
    }

    /**
     * @param itemFileName the itemFileName to set
     */
    public void setItemFileName(String itemFileName) {
        this.itemFileName = itemFileName;
    }

    /**
     * @return the itemFileType
     */
    public String getItemFileType() {
        return itemFileType;
    }

    /**
     * @param itemFileType the itemFileType to set
     */
    public void setItemFileType(String itemFileType) {
        this.itemFileType = itemFileType;
    }
    
    @Override
    public String toString() {
        return super.toString() +
                " - File name: " + getItemFileName() +                
                " - File type: " + getItemFileType();
    }  

    @Override
    String toString(char delimiter) {
        String output = QUOTE + super.getItemTitle() + QUOTE + delimiter +
                        QUOTE + getItemFileName() + QUOTE + delimiter + 
                        QUOTE + getItemFileType() + EOLN;
        return output;
    }

    
}
