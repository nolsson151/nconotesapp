package model;

import java.io.Serializable;

public abstract class Item implements Serializable {

    static final char EOLN='\n';       
    static final String QUOTE="\"";
    
    private String itemTitle;
    
    /**
     *
     */
    public Item() {
        this.itemTitle = null;
    }
    
    /**
     *
     * @param itemTitle
     */
    public Item(String itemTitle) {
        this.itemTitle = itemTitle;
    }
    
    /**
     * @return the itemTitle
     */
    public String getItemTitle() {
        return itemTitle;
    }

    /**
     * @param itemTitle the itemTitle to set
     */
    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }
    
    @Override
    public String toString() {
        return "Item: " + getItemTitle();
    }

    abstract String toString(char delimiter);

    
}
