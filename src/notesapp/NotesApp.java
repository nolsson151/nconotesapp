package notesapp;

import controllers.NotesController;
import controllers.TestNotesController;


public class NotesApp {

    /**
     *
     */
    public static void run() {        
        System.out.println("Notes App\n=========\n\n");
        
        //To run the normal application, uncomment code below.
        
       NotesController notesController = new NotesController();
       notesController.run();
        
//        TestNotesController testNotesController = new TestNotesController();   
//        testNotesController.run();
        
        System.out.println("Thank you for using Notes App. Good bye.\n");        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        NotesApp notesApp = new NotesApp();
        notesApp.run();
    }
    
}
