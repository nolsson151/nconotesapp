package repositories;

import daos.DAOImpl;
import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;


public class Repository<T extends RepositoryObject> implements RepositoryInterface<T>, Serializable {
    private Set<T> items;    
    
    public Repository() {
        this.items = new TreeSet<>();       
    }
    
    public Repository(Set<T> items) {        
        this.items = items;
    }
    
    public Repository(String filename) {
        this();
        DAOImpl dao = new DAOImpl();
        this.items = dao.load(filename).getItems();
    }
    
    
    @Override
    public Set<T> getItems() {        
        return this.items;
    }
    
    @Override
    public void setItems(Set<T> items) {        
        this.items = items;
    }
    
    @Override
    public void add(T item) {
        this.items.add(item);
    }
       
    @Override
    public void remove(int id) {
        Predicate<T> predicate = e->e.getId() == id;       
        this.items.removeIf(predicate);
    }
    
    @Override
    public T getItem(int id) {
        for (T item:this.items) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }
    
    @Override
    public String toString() {
        return "\nItems: " + this.items;
    }    
    
    @Override
    public void store(String filename) {       
        DAOImpl dao = new DAOImpl();
        dao.store(filename, this);
    }        

}
