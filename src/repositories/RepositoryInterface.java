package repositories;

import java.util.Set;
import java.util.TreeSet;
import model.Note;

public interface RepositoryInterface<T extends RepositoryObject> {

    void add(T item);

    T getItem(int id);

    Set<T> getItems();
    
    void remove(int id);

    void setItems(Set<T> items);
    
    void store(String filename);

    String toString();
    
}
