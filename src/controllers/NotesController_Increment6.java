package controllers;

import helpers.InputHelper;
import java.util.Calendar;
import java.util.Iterator;
import model.AttachmentItem;
import repositories.Repository;
import model.Note;
import model.TextItem;

public class NotesController_Increment6 {
    private final Repository<Note> repository;
    
    /**
     *
     */
        
    public NotesController_Increment6() {
        InputHelper inputHelper = new InputHelper();
        char c = inputHelper.readCharacter("Load an already existing Note (Y/N)?");
        if (c == 'Y' || c == 'y') {
            String fileName = inputHelper.readString("Enter filename");
            this.repository = new Repository<>(fileName);
        }
        else {
            this.repository = new Repository<>();
            addNotes();
        }
    }
   
    /**
     *
     */
    public void run() {
        boolean finished = false;
        
        do {
            char choice = displayNotesMenu();
            switch (choice) {
                case 'A': 
                    addNote();
                    break;
                case 'B': 
                    addItem();
                    break; 
                case 'C': 
                    removeItem();
                    break; 
                case 'D': 
                    listNotes();
                    break;
                case 'E': 
                    listNotesInDateModifiedOrder();
                    break;                    
                case 'F': 
                    listNotesWhichMatchSubject();
                    break;
                case 'Q': 
                    finished = true;
            }
        } while (!finished);
    }
    
    private char displayNotesMenu() {
        InputHelper inputHelper = new InputHelper();
        System.out.print("\nA. Add Note");
        System.out.print("\tB. Add Item");
        System.out.print("\tC. Remove Item");
        System.out.print("\tD. List Notes");        
        System.out.print("\nE. List Notes In Date Created Order");
        System.out.print("\tF. List Notes Which Match Subject");       
        System.out.print("\tQ. Quit\n");         
        return inputHelper.readCharacter("Enter choice", "ABCDEFQ");
    }    
    
    private void addNote() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Add Note");
        String noteHeading = inputHelper.readString("Enter Note Heading");
        String noteSubject = inputHelper.readString("Enter Note Subject");
        Note note = new Note(noteHeading, noteSubject);
        repository.add(note);
        System.out.format("\033[31m%s\033[0m%n", "========");        
    }
    
    private void addNotes(){
        Note n1 = new Note("I am selling Cars", "Cars");
        repository.add(n1);
        Note n2 = new Note("Looking for work?", "Employment");
        repository.add(n2);
        Note n3 = new Note("Check out my mixtape", "Music");
        repository.add(n3);
    }
    
    private void addItem() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Add Item");
        boolean validId = false;
        Note requiredNote = null;
        do{
            int searchId = inputHelper.readInt("Enter Note Id");
            requiredNote = repository.getItem(searchId);
            if (requiredNote != null){
                validId = true;
            }
        }
        while(!validId);
        System.out.println(requiredNote.toString());
        char c = inputHelper.readCharacter("Text Item or Attachment Item (T/A)?");
        if(c == 'T' || c == 't'){
            String itemTitle = inputHelper.readString("Enter Item Title ");
            String itemText = inputHelper.readString("Enter Item Text");
            TextItem newTextItem = new TextItem(itemTitle, itemTitle);
            requiredNote.addItem(newTextItem);
        }
        else{
            String itemTitle = inputHelper.readString("Enter Item Title");
            String itemText = inputHelper.readString("Enter Item Text");
            String itemFileType = inputHelper.readString("Enter Item File Type");
            AttachmentItem newAttachmentItem = new AttachmentItem(itemTitle, itemText, itemFileType);
            requiredNote.addItem(newAttachmentItem);
        }       
        System.out.format("\033[31m%s\033[0m%n", "========");         
    }    
    
    private void removeItem() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Remove Item: ");
        boolean validId = false;
        Note requiredNote = null;
        do{
            int searchId = inputHelper.readInt("Enter Note Id");
            requiredNote = repository.getItem(searchId);
            if (requiredNote != null){
                validId = true;
            }
        }
        while (!validId);
        System.out.println("Note\n=============\n" + requiredNote);
        int itemToRemove = inputHelper.readInt("Enter Item Array Index to remove", requiredNote.getNoItems(), 0);
        requiredNote.removeItem(itemToRemove);
        System.out.format("\033[31m%s\033[0m%n", "===========");     
    }    
    
    private void listNotes() {        
        System.out.format("\033[31m%s\033[0m%n", "Notes");
        Iterator it = repository.getItems().iterator();
        while (it.hasNext()){
            Note n = (Note) it.next();
            System.out.println(n);
        }
        System.out.format("\033[31m%s\033[0m%n", "=====");         
    } 

    private void listNotesInDateModifiedOrder() {
        System.out.format("\033[31m%s\033[0m%n", "Date Modified Order");
        Calendar dateModified = null;
        Iterator it = repository.getItems().iterator();
        while (it.hasNext()){
            Note n = (Note) it.next();
            if(n.match(note -> note.getDateModified().equals(dateModified)));
            System.out.println(n);   
        }
        System.out.format("\033[31m%s\033[0m%n", "==================");     
    }    
    
    private void listNotesWhichMatchSubject() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Subject");
        String subject = inputHelper.readString("Enter a Subject to search for");
        repository.getItems()
                  .stream()
                  .filter(n -> n.getNoteSubject().equals(subject))
                  .forEach(string -> System.out.println(string));
        System.out.format("\033[31m%s\033[0m%n", "=======");          
    }
}