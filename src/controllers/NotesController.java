package controllers;

import helpers.InputHelper;
;
import model.AttachmentItem;
import repositories.Repository;
import model.Note;
import model.TextItem;

public class NotesController {
    private final Repository<Note> repository;
    
    /**
     *
     */
        
    public NotesController() {
        InputHelper inputHelper = new InputHelper();
        char c = inputHelper.readCharacter("Load an already existing Note (Y/N)?");
        if (c == 'Y' || c == 'y') {
            String fileName = inputHelper.readString("Enter filename");
            this.repository = new Repository<>(fileName);
        }
        else {
            this.repository = new Repository<>();
            addNotes();
        }
    }
   
    /**
     *
     */
    public void run() {
        boolean finished = false;
        
        do {
            char choice = displayNotesMenu();
            switch (choice) {
                case 'A': 
                    addNote();
                    break;
                case 'B': 
                    addItem();
                    break; 
                case 'C': 
                    removeItem();
                    break; 
                case 'D': 
                    listNotes();
                    break;
                case 'E': 
                    listNotesInDateModifiedOrder();
                    break;                    
                case 'F': 
                    listNotesWhichMatchSubject();
                    break;
                case 'Q': 
                    InputHelper inputHelper = new InputHelper();                    
                    String fileName = inputHelper.readString("Enter filename to save files to:"); 
                    repository.store(fileName);
                    finished = true;
            }
        } while (!finished);
    }
    
    private char displayNotesMenu() {
        InputHelper inputHelper = new InputHelper();
        System.out.print("\nA. Add Note");
        System.out.print("\tB. Add Item");
        System.out.print("\tC. Remove Item");
        System.out.print("\tD. List Notes");        
        System.out.print("\nE. List Notes In Date Modified Order");
        System.out.print("\tF. List Notes Which Match Subject");       
        System.out.print("\tQ. Quit\n");         
        return inputHelper.readCharacter("Enter choice", "ABCDEFQ");
    }    
    
    private void addNote() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Add Note");
        String noteHeading = inputHelper.readString("Enter Note Heading");
        String noteSubject = inputHelper.readString("Enter Note Subject");
        Note note = new Note(noteHeading, noteSubject);
        repository.add(note);
        System.out.format("\033[31m%s\033[0m%n", "========");        
    }
    
    private void addNotes(){
        Note n1 = new Note("First Note", "Frist Subject");
        repository.add(n1);
        TextItem t1 = new TextItem("First Item", "First Item Text");
        TextItem t2 = new TextItem("Second Item", "Second Item Text");
        AttachmentItem a1 = new AttachmentItem("Third Item", "Third Item File Name", "Third Item File Type");
        n1.addItem(t1);
        n1.addItem(t2);
        n1.addItem(a1);
        Note n2 = new Note("Second Note", "Second Subject");
        repository.add(n2);
        TextItem t3 = new TextItem("First Item", "First Item Text");
        n2.addItem(t3);
        
    }
    
    private void addItem() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Add Item");
        boolean validId = false;
        Note requiredNote = null;
        do{
            int searchId = inputHelper.readInt("Enter Note Id");
            requiredNote = repository.getItem(searchId);
            if (requiredNote != null){
                validId = true;
            }
        }
        while(!validId);
        System.out.println(requiredNote.toString());
        char c = inputHelper.readCharacter("Text Item or Attachment Item (T/A)?");
        if(c == 'T' || c == 't'){
            String itemTitle = inputHelper.readString("Enter Item Title ");
            String itemText = inputHelper.readString("Enter Item Text");
            TextItem newTextItem = new TextItem(itemTitle, itemTitle);
            requiredNote.addItem(newTextItem);
        }
        else{
            String itemTitle = inputHelper.readString("Enter Item Title");
            String itemFileName = inputHelper.readString("Enter Item Text");
            String itemFileType = inputHelper.readString("Enter Item File Type");
            AttachmentItem newAttachmentItem = new AttachmentItem(itemTitle, itemFileName, itemFileType);
            requiredNote.addItem(newAttachmentItem);
        }       
        System.out.format("\033[31m%s\033[0m%n", "========");         
    }    
    
    private void removeItem() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Remove Item: ");
        boolean validId = false;
        Note requiredNote = null;
        do{
            int searchId = inputHelper.readInt("Enter Note Id");
            requiredNote = repository.getItem(searchId);
            if (requiredNote != null){
                validId = true;
            }
        }
        while (!validId);
        System.out.println("Note\n=============\n" + requiredNote);
        int itemToRemove = inputHelper.readInt("Enter Item Array Index to remove", requiredNote.getNoItems(), 0);
        requiredNote.removeItem(itemToRemove);
        System.out.format("\033[31m%s\033[0m%n", "===========");     
    }    
    
    private void listNotes() {        
        System.out.format("\033[31m%s\033[0m%n", "Notes");
        repository.getItems()
                  .stream()
                  .forEach(n -> System.out.println(n));
        System.out.format("\033[31m%s\033[0m%n", "=====");         
    } 

    private void listNotesInDateModifiedOrder() {
        System.out.format("\033[31m%s\033[0m%n", "Date Modified Order");
        repository.getItems()
                  .stream()
                  .sorted(Note.DateModifiedComparator)
                  .forEach(n -> System.out.println(n));
        System.out.format("\033[31m%s\033[0m%n", "==================");     
    }    
    
    private void listNotesWhichMatchSubject() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Subject");
        String subject = inputHelper.readString("Enter a Subject to search for");
        repository.getItems()
                  .stream()
                  .filter(n -> n.getNoteSubject().equals(subject))                  
                  .forEach(string -> System.out.println(string));
        System.out.format("\033[31m%s\033[0m%n", "=======");          
    }
}
