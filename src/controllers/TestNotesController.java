package controllers;

import helpers.InputHelper;
;
import model.AttachmentItem;
import repositories.Repository;
import model.Note;
import model.TextItem;

public class TestNotesController {
    private final Repository<Note> repository;
    
    /**
     *
     */
        
    public TestNotesController() {
            this.repository = new Repository<>();
            addNotes();   
    }
   
    /**
     *
     */
    public void run() {
        displayNotesMenu();
        addNote();
        displayNotesMenu();
        addItem();
        displayNotesMenu();
        removeItem();
        displayNotesMenu();
        listNotes();
        displayNotesMenu();
        listNotesInDateModifiedOrder();
        displayNotesMenu();
        listNotesWhichMatchSubject();
        displayNotesMenu();
        System.out.println("Enter filename");
        System.out.println("notes.dat");
        String fileName = "notes.dat"; 
        repository.store(fileName);

    }
    
    private void displayNotesMenu() {
        System.out.print("\nA. Add Note");
        System.out.print("\tB. Add Item");
        System.out.print("\tC. Remove Item");
        System.out.print("\tD. List Notes");        
        System.out.print("\nE. List Notes In Date Modified Order");
        System.out.print("\tF. List Notes Which Match Subject");       
        System.out.print("\tQ. Quit\n");         
    }    
    
    private void addNote() {
        System.out.format("\033[31m%s\033[0m%n", "Add Note");
        System.out.println("Enter Note Heading");
        System.out.println("Third Note");
        System.out.println("Enter Note Subject");
        System.out.println("Third Subject");
        String noteHeading = "Third Note";
        String noteSubject = "Third Subject";
        Note note = new Note(noteHeading, noteSubject);
        repository.add(note);
        System.out.format("\033[31m%s\033[0m%n", "========");        
    }
    
    private void addNotes(){
        Note n1 = new Note("First Note", "Frist Subject");
        repository.add(n1);
        TextItem t1 = new TextItem("First Item", "First Item Text");
        TextItem t2 = new TextItem("Second Item", "Second Item Text");
        AttachmentItem a1 = new AttachmentItem("Third Item", "Third Item File Name", "Third Item File Type");
        n1.addItem(t1);
        n1.addItem(t2);
        n1.addItem(a1);
        Note n2 = new Note("Second Note", "Second Subject");
        repository.add(n2);
        TextItem t3 = new TextItem("First Item", "First Item Text");
        n2.addItem(t3);
        
    }
    
    private void addItem() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Add Item");
        boolean validId = false;
        Note requiredNote = null;
        do{
            int searchId = 2;
            System.out.println("2"); 
            requiredNote = repository.getItem(searchId);
            if (requiredNote != null){
                validId = true;
            }
        }
        while(!validId);
        System.out.println(requiredNote.toString());
        System.out.println("Text Item or Attachment Item (T/A)?");
        char c = 'T';
        
        if(c == 'T' || c == 't'){
            System.out.println("Enter Item Title");
            System.out.println("Second Item");
            System.out.println("Enter Item Text");
            System.out.println("Second Item Text");
            String itemTitle = "Second Item";
            String itemText = "Second Item Text";
            TextItem newTextItem = new TextItem(itemTitle, itemText);
            requiredNote.addItem(newTextItem);
        }
        else{
            String itemTitle = inputHelper.readString("Enter Item Title");
            String itemFileName = inputHelper.readString("Enter Item Text");
            String itemFileType = inputHelper.readString("Enter Item File Type");
            AttachmentItem newAttachmentItem = new AttachmentItem(itemTitle, itemFileName, itemFileType);
            requiredNote.addItem(newAttachmentItem);
        }       
        System.out.format("\033[31m%s\033[0m%n", "========");         
    }    
    
    private void removeItem() {
        System.out.format("\033[31m%s\033[0m%n", "Remove Item: ");
        boolean validId = false;
        Note requiredNote = null;
        do{
            System.out.println("Enter Note Id");
            System.out.println("1");
            int searchId = 1;
            requiredNote = repository.getItem(searchId);
            if (requiredNote != null){
                validId = true;
            }
        }
        while (!validId);
        System.out.println("Note\n=============\n" + requiredNote);
        System.out.println("Enter Item Array Index to remove");
        System.out.println("1");
        int itemToRemove = 1;
        requiredNote.removeItem(itemToRemove);
        System.out.format("\033[31m%s\033[0m%n", "===========");     
    }    
    
    private void listNotes() {        
        System.out.format("\033[31m%s\033[0m%n", "Notes");
        repository.getItems()
                  .stream()
                  .forEach(n -> System.out.println(n));
        System.out.format("\033[31m%s\033[0m%n", "=====");         
    } 

    private void listNotesInDateModifiedOrder() {
        System.out.format("\033[31m%s\033[0m%n", "Date Modified Order");
        repository.getItems()
                  .stream()
                  .sorted(Note.DateModifiedComparator)
                  .forEach(n -> System.out.println(n));
        System.out.format("\033[31m%s\033[0m%n", "==================");     
    }    
    
    private void listNotesWhichMatchSubject() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Subject");
        System.out.println("Enter a Subject to search for");
        System.out.println("Second Subject");
        String subject = "Second Subject";
        repository.getItems()
                  .stream()
                  .filter(n -> n.getNoteSubject().equals(subject))                  
                  .forEach(string -> System.out.println(string));
        System.out.format("\033[31m%s\033[0m%n", "=======");          
    }
}