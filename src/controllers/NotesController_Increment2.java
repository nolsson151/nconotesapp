package controllers;

import helpers.InputHelper;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import repositories.Repository;
import model.Note;

public class NotesController_Increment2 {
    private final Repository<Note> repository;
    
    /**
     *
     */
        
    public NotesController_Increment2() {
        InputHelper inputHelper = new InputHelper();
        char c = inputHelper.readCharacter("Load an already existing Note (Y/N)?");
        if (c == 'Y' || c == 'y') {
            String fileName = inputHelper.readString("Enter filename");
            this.repository = new Repository<>(fileName);
        }
        else {
            this.repository = new Repository<>();
        }
    }
   
    /**
     *
     */
    public void run() {
        boolean finished = false;
        
        do {
            char choice = displayNotesMenu();
            switch (choice) {
                case 'A': 
                    addNote();
                    break;
                case 'B': 
                    addItem();
                    break; 
                case 'C': 
                    removeItem();
                    break; 
                case 'D': 
                    listNotes();
                    break;
                case 'E': 
                    listNotesInDateModifiedOrder();
                    break;                    
                case 'F': 
                    listNotesWhichMatchSubject();
                    break;
                case 'Q': 
                    finished = true;
            }
        } while (!finished);
    }
    
    private char displayNotesMenu() {
        InputHelper inputHelper = new InputHelper();
        System.out.print("\nA. Add Note");
        System.out.print("\tB. Add Item");
        System.out.print("\tC. Remove Item");
        System.out.print("\tD. List Notes");        
        System.out.print("\nE. List Notes In Date Created Order");
        System.out.print("\tF. List Notes Which Match Subject");       
        System.out.print("\tQ. Quit\n");         
        return inputHelper.readCharacter("Enter choice", "ABCDEFQ");
    }    
    
    private void addNote() {
        InputHelper inputHelper = new InputHelper();
        System.out.format("\033[31m%s\033[0m%n", "Add Note");
        String noteHeading = inputHelper.readString("Enter Note Heading");
        String noteSubject = inputHelper.readString("Enter Note Subject");
        Note note = new Note(noteHeading, noteSubject);
        repository.add(note);
        System.out.format("\033[31m%s\033[0m%n", "========");        
    }
    
    private void addItem() {        
        System.out.format("\033[31m%s\033[0m%n", "Add Item");
        System.out.format("\033[31m%s\033[0m%n", "========");         
    }    
    
    private void removeItem() {        
        System.out.format("\033[31m%s\033[0m%n", "Remove Item: ");
        System.out.format("\033[31m%s\033[0m%n", "===========");     
    }    
    
    private void listNotes() {        
        System.out.format("\033[31m%s\033[0m%n", "Notes");
        Iterator it = repository.getItems().iterator();
        while (it.hasNext()){
            Note n = (Note) it.next();
            System.out.println(n);
        }
        System.out.format("\033[31m%s\033[0m%n", "=====");         
    } 

    private void listNotesInDateModifiedOrder() {        
        System.out.format("\033[31m%s\033[0m%n", "Date Modified Order");
        System.out.format("\033[31m%s\033[0m%n", "==================");     
    }    
    
    private void listNotesWhichMatchSubject() {
        System.out.format("\033[31m%s\033[0m%n", "Subject");
        System.out.format("\033[31m%s\033[0m%n", "=======");          
    }        
}